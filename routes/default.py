"""The Endpoints to manage the default routes"""

from flask import jsonify, abort, request, Blueprint
from flask_api import status
DEFAULT_API = Blueprint('DEFAULT_API', __name__)


def get_blueprint():
    """Return the blueprint for the main app module"""
    return DEFAULT_API


@DEFAULT_API.route('/', methods=['GET'])
def get():
    # abort(status.HTTP_400_BAD_REQUEST)
    pass

@DEFAULT_API.route('/', methods=['POST'])
def post():
    pass

@DEFAULT_API.route('/', methods=['PUT'])
def put():
    pass

@DEFAULT_API.route('/', methods=['DELETE'])
def delete():
    pass