# Project
## Download
```
sudo apt install git
git clone url
```
## Modules
* `Module 1`
* `TEST`

## Top Level Directories
* `configs/` - contains configuration parameters common to all sub modules in the project
* `data/` - contains sample data for example usage and to run test cases
* `logs/` - contains log files generated
* `tmp/` - contains images

## Required Libraries
    ```
    # GENERAL pre-requisites

    python3.7

    # for default module
    dataclasses==0.7
    Flask==1.1.1
    Flask-Cors==3.0.8
    flask-swagger-ui==3.20.9
    pytz==2019.3
    requests==2.22.0
    ```
    ```
    # TEST
    pytest==5.2.1
    ```

## Installation
#### Steps to installation of different modules

    
Setup python3, pip and virtual environments

    # Setup python3 and pip

    $ sudo apt-get install -y python3-dev python3-pip 
    $ wget https://bootstrap.pypa.io/get-pip.py 
    $ sudo python3 get-pip.py 


- to install the the project
    ```
    $ pip install -r requirements.txt
    ```
- for TEST module nothing is needed to be installed. Required libraries will be installed with default module


### Must `TODO` before running python server-
* Update device specific values in `configs/project_config.py`
    ```
    PROJECT_DIR : str = HOME_DIR + '[path/to/root-folder]'
    ```

## Run python and node servers
    ```
    # to run python server -
    $ cd [PROJECT_DIR]/
    $ python app.py
    ```

* To run test cases follow the `README.md` in `test/` directory