import argparse
import os
import sys
import logging

from waitress import serve
from flask import Flask, jsonify, make_response
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from werkzeug import exceptions as HTTP_EXCEPTIONS

from routes import default
from http_error_response import HTTPErrorResponse
from configs.project_config import ProjectConfig
from utils import get_log_handler

APP = Flask(__name__)

### swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL
)
APP.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
### end swagger specific ###

APP.register_blueprint(default.get_blueprint(), url_prefix='')

@APP.errorhandler(HTTP_EXCEPTIONS.HTTPException)
def handle_error(_error):
    response, status_code = HTTPErrorResponse.get_default_response(_error)
    return make_response(response, status_code)

def run():
    # create AppLogger from here
    logger = logging.getLogger(ProjectConfig().LOGGER_NAME)
    handler = get_log_handler()
    #handler.rotator = custom_rotator
    logger.addHandler(handler)
    logger.setLevel(ProjectConfig().LOG_LEVEL)
    logger.info('Logging initiated')

    PARSER = argparse.ArgumentParser(description="iDREAMS-rest-server")
    PARSER.add_argument('--debug', action='store_true', help="Use flask debug/dev mode with file change reloading")
    ARGS = PARSER.parse_args()
    PORT = int(os.environ.get('PORT', 8080))

    if ARGS.debug:
        logger.info("Running in debug mode")
        CORS = CORS(APP)
        # running werkzeug's flask server instead of wsgi (waitress)
        APP.run(host='0.0.0.0', port=PORT, debug=True)
        logger.info("Flask dev server started")
    else:
        # running wsgi (waitress) standard server
        logger.info("Running in production mode")
        serve(APP)
        logger.info("WSGI server started")

if __name__ == '__main__':
    run()