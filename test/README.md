# PYTEST testing implementation

## Requirements to know / follow - 
* Do not create any more `test` folder
* All test cases should be in `test` folder that is already there
* New files containing new cases should have names starting with `test_[module-name]_` .Complete filename example-
    ```
    test_service.py
    ```
* Name of the function implementing a test case should start with `test_`
* `@pytest.mark.[group_name]` is optional before any test function. It helps to group multiple test cases together and gives a way to run only that group of functions while testing.

### Must to-do before testing-
* Update project directory in `configs/project_config.py`
    ```
    PROJECT_DIR : str = HOME_DIR + '[path/to/project-folder]' 
    ```
    For testing on windows - 
    ```
    PROJECT_DIR : str = '[path/to/project-folder-on-windows]' 
    ```

## Structure of a file containing test cases

* Filename: test_[whatever].py

    ```
    import pytest

    from [whatever].[module].[you].[need] import [WhatEverClassOrFunctions]

    def setup_module(module):
        """Optional. Setup any state which is required for tests of the given module. """
        # code
    
    def teardown_module(module):
        """Optional. Teardown any state that was setup during testing. """
        # code

    @pytest.mark.[groupname]
    def test_[function_name]():
        # code for testing


    ```

## To run test cases:
* Open a terminal in the project root folder and then-
    ```
    # to run all test cases available in the test folder across all files

    $ pytest

    # to run a specific group of test cases - 

    $ pytest -m [groupname] -v

    # to disable warnings

    $ pytest --disable-warnings

    ```