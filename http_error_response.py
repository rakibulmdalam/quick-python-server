from flask_api import status

class HTTPErrorResponse:
    @staticmethod
    def get_default_response(exception_type):
        exception_str = str(exception_type)
        split = exception_str.split(' ', 1)
        response = {"error": split[1]}
        status_code = split[0]
        return response, status_code

    @staticmethod
    def get_customize_response(exception_type):
        response = ''
        status_code = 0
        if type(exception_type) == 'werkzeug.exceptions.BadRequest':
            status_code = status.HTTP_400_BAD_REQUEST
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.Unauthorized':
            status_code = status.HTTP_401_UNAUTHORIZED
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.Forbidden':
            status_code = status.HTTP_403_FORBIDDEN
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.NotFound':
            status_code = status.HTTP_404_NOT_FOUND
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.MethodNotAllowed':
            status_code = status.HTTP_405_METHOD_NOT_ALLOWED
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.NotAcceptable':
            status_code = status.HTTP_406_NOT_ACCEPTABLE
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.RequestTimeout':
            status_code = status.HTTP_408_REQUEST_TIMEOUT
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.Conflict':
            status_code = status.HTTP_409_CONFLICT
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.InternalServerError':
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.NotImplemented':
            status_code = status.HTTP_501_NOT_IMPLEMENTED
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.BadGateway':
            status_code = status.HTTP_502_BAD_GATEWAY
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.ServiceUnavailable':
            status_code = status.HTTP_503_SERVICE_UNAVAILABLE
            response = {'error': ''}
        elif type(exception_type) == 'werkzeug.exceptions.GatewayTimeout':
            status_code = status.HTTP_504_GATEWAY_TIMEOUT
            response = {'error': 'Gateway Timeout'}
        elif type(exception_type) == 'werkzeug.exceptions.HTTPVersionNotSupported':
            status_code = status.HTTP_505_HTTP_VERSION_NOT_SUPPORTED
            response = {'error': 'HTTP Version Not Supported'}
        
        return response, status_code