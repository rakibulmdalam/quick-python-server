{
    "swagger": "2.0",
    "info": {
      "description": "This is a sample ORDER ENTRY server",
      "version": "1.0.0",
      "title": "Simuated Order Entry Server",
      "termsOfService": "http://swagger.io/terms/",
      "contact": {
        "email": "rakibul.alam@s4dx.com"
      },
      "license": {
        "name": "Apache 2.0",
        "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
      }
    },
    "host": "simserver.s4dx.eu",
    "basePath": "/",
    "tags": [
      {
        "name": "order",
        "description": "Everything about hcp Order"
      }
    ],
    "schemes": [
      "https",
      "http"
    ],
    "paths": {
      "/order": {
        "post": {
          "tags": [
            "order"
          ],
          "summary": "Add a new order to the system",
          "description": "",
          "operationId": "addOrder",
          "consumes": [
            "application/json"
          ],
          "produces": [
            "application/json"
          ],
          "parameters": [
            {
              "in": "body",
              "name": "body",
              "description": "Order object that needs to be added to the system",
              "required": true,
              "schema": {
                "$ref": "#/definitions/Order"
              }
            }
          ],
          "responses": {
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            }
          }
        },
        "put": {
          "tags": [
            "order"
          ],
          "summary": "Update an existing order",
          "description": "",
          "operationId": "updateOrder",
          "consumes": [
            "application/json"
          ],
          "produces": [
            "application/json"
          ],
          "parameters": [
            {
              "in": "body",
              "name": "body",
              "description": "Order object that needs to be updated in the system",
              "required": true,
              "schema": {
                "$ref": "#/definitions/Order"
              }
            }
          ],
          "responses": {
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            },
            "404": {
              "description": "error: Not_Found: the requested resource not found in the server"
            }
          }
        },
        "get": {
          "tags": [
            "order"
          ],
          "summary": "Find all orders",
          "description": "listing all orders from the Order entry system",
          "operationId": "findOrders",
          "produces": [
            "application/json"
          ],
          "responses": {
            "200": {
              "description": "success",
              "schema": {
                "type": "array",
                "items": {
                  "$ref": "#/definitions/Order"
                }
              }
            },
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            }
          }
        }
      },
      "/order/{_order_id}": {
        "get": {
          "tags": [
            "order"
          ],
          "summary": "Find order by ID",
          "description": "Returns a single order",
          "operationId": "getOrderById",
          "produces": [
            "application/json"
          ],
          "parameters": [
            {
              "name": "_order_id",
              "in": "path",
              "description": "ID of Order to return",
              "required": true,
              "type": "integer",
              "format": "int64"
            }
          ],
          "responses": {
            "200": {
              "description": "successful",
              "schema": {
                "$ref": "#/definitions/Order"
              }
            },
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            },
            "404": {
              "description": "error: Not_Found: the requested resource not found in the server"
            }
          }
        },
        "delete": {
          "tags": [
            "order"
          ],
          "summary": "Deletes a order",
          "description": "",
          "operationId": "deleteOrder",
          "produces": [
            "application/json"
          ],
          "parameters": [
            {
              "name": "_order_id",
              "in": "path",
              "description": "Order id to delete",
              "required": true,
              "type": "integer",
              "format": "int64"
            }
          ],
          "responses": {
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            },
            "404": {
              "description": "error: Not_Found: the requested resource not found in the server"
            }
          }
        }
      },
      "/order/hcp/{_hcp_id}": {
        "get": {
          "tags": [
            "order"
          ],
          "summary": "Find order by hcp ID",
          "description": "Returns a list of orders filtered by hcp_id",
          "operationId": "getOrderByHCPId",
          "produces": [
            "application/json"
          ],
          "parameters": [
            {
              "name": "_hcp_id",
              "in": "path",
              "description": "ID of pet to return",
              "required": true,
              "type": "integer",
              "format": "int64"
            }
          ],
          "responses": {
            "200": {
              "description": "successful",
              "schema": {
                "$ref": "#/definitions/Order"
              }
            },
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            },
            "404": {
              "description": "error: Not_Found: the requested resource not found in the server"
            }
          }
        }
      },
      "/order/hcp/{_hcp_id}/since/{_since_timestamp}": {
        "get": {
          "tags": [
            "order"
          ],
          "summary": "Find order by hcp ID and in a time range starting from given timestamp in path",
          "description": "Returns a list of orders filtered by hcp_id",
          "operationId": "getOrderByHCPIdAndTimestamp",
          "produces": [
            "application/xml",
            "application/json"
          ],
          "parameters": [
            {
              "name": "_hcp_id",
              "in": "path",
              "description": "ID pf HCP for which orders are queried for",
              "required": true,
              "type": "string"
            },
            {
              "name": "_since_timestamp",
              "in": "path",
              "description": "Timestamp of the range of time in the past",
              "required": true,
              "type": "string",
              "format": "int64"
            }
          ],
          "responses": {
            "200": {
              "description": "successful operation",
              "schema": {
                "$ref": "#/definitions/Order"
              }
            },
            "400": {
              "description": "error: Bad_Request: the server could not understand the request"
            },
            "404": {
              "description": "error: Not_Found: the requested resource not found in the server"
            }
          }
        }
      }
    },
    "definitions": {
      "Order": {
        "type": "object",
        "properties": {
          "orderID": {
            "type": "string",
            "format": "string"
          },
          "hcpID": {
            "type": "string",
            "format": "string"
          },
          "timestamp": {
            "type": "string",
            "format": "date-time"
          },
          "samples": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "sampleID": {
                  "type": "string",
                  "format": "string"
                },
                "tubeTypeID": {
                  "type": "string",
                  "format": "string"
                }
              }
            }
          }
        },
        "xml": {
          "name": "Order"
        }
      }
    }
  }