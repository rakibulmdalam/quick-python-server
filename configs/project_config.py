from dataclasses import dataclass

import logging
import os
import sys

@dataclass
class ProjectConfig:
    # For Windows,
    #HOME_DIR : str = os.environ['HOMEPATH']
    # For Linux,
    HOME_DIR : str = os.environ['HOME']
    PROJECT_DIR : str = HOME_DIR + '/path/to/project/' 
    
    LOG_DIR : str = PROJECT_DIR + 'logs/'
    LOG_FILE : str = 'project.log'
    LOGGER_NAME : str = 'project_logger'
    LOG_LEVEL : str = logging.DEBUG
    LOG_FORMATTER : str = '%(asctime)s - %(name)s - %(levelname)s - [%(filename)s:%(lineno)s - %(funcName)s() ] - %(message)s'
    LOG_ROTATE_WHEN : str = 'h' # Possible values for LOG_ROTATE_WHEN > 's', 'm', 'h', 'd', 'midnight'
    LOG_ROTATE_INTERVAL : int = 4 # currently every 4 hours log files will be rotated
    LOG_ROTATE_BACKUPCOUNT : int = 0
