import argparse
import os
import sys
import multiprocessing as mp

from configs.project_config import ProjectConfig

path = ProjectConfig().PROJECT_DIR
exclude = [".git", "__pycache__", "node_modules", "static"]
DIRS = [x[0] for x in os.walk(path)]
for d in DIRS:
    split_d = d.split('/')
    common = list(set(split_d).intersection(exclude))
    if len(common) < 1:
        sys.path.append(d+'/')

import server

def go():
    pyserver = mp.Process(target=server.run())
    try:
        pyserver.start()
    except Exception as ex: 
        print(ex)
    finally:
        pyserver.terminate()

if __name__ == '__main__':
    go()